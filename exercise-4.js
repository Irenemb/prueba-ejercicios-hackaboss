/**
 * #############################
 * ##  E J E R C I C I O   4  ##
 * #############################
 *
 * Obtener un array con los nombres de todos los municipios de la provincia de Lugo (Galicia)
 * ordenados por orden alfabético inverso (de la Z a la A). Deberás hacer uso de fetch y
 * async / await.
 *
 * Para facilitarte esta tarea dispones de la siguiente API: https://www.el-tiempo.net/api
 *
 * Debes entrar en la web y leer la documentación para encontrar la URL que necesitas. En
 * este caso es bastante simple e intuitivo. ¡A por todas! ;)
 *
 */

'use strict';

const fuenteDatos = 'https://www.el-tiempo.net/api/json/v2/provincias/27/municipios';

async function getTownsLugo(url) {
  const respuestaApi = await fetch(url);
  console.log(respuestaApi);
  const respuestaDeFetch = await respuestaApi.json();
  console.log(respuestaDeFetch);
}

getTownsLugo(fuenteDatos);

async function getNameOfMunicipio(url) {
  const datosLugo = await (await fetch(url)).json();
  const municipiosLugo = await (await fetch(datosLugo.municipios)).json();
  console.log(printMunicipios(municipiosLugo));
}

//hasta aquí parece que funciona

getNameOfMunicipio(fuenteDatos);

function printMunicipios(municipio) {
  for (const nombre of municipio) {
    console.log(nombre);
  }
}
